fn main() {
    let mut timer = rusty_tomato::Timer::new();
    timer.start().unwrap_or_else(|err| {
        eprintln!("{}", err.to_string());
        std::process::exit(1);
    });
    timer.get_results().unwrap_or_else(|err| {
        eprintln!("{}", err.to_string());
        std::process::exit(1);
    });
}
