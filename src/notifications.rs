use std::io;

pub fn notify_with(msg: &str) -> io::Result<()> {
    match notify_rust::Notification::new()
        .appname("Rusty Tomato")
        .body(msg)
        .timeout(notify_rust::Timeout::Default)
        .show()
        .err()
    {
        Some(error) => {
            return Err(io::Error::new(
                io::ErrorKind::Interrupted,
                error.to_string(),
            ))
        }
        None => Ok(()),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn notify() {
        assert_eq!((), notify_with("Test").unwrap());
    }
}
