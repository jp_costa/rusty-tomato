use std::{io, path, process};

pub fn ring_alarm() -> io::Result<()> {
    match process::Command::new("paplay")
        .arg(fetch_audio_file()?)
        .output()
    {
        Ok(_) => Ok(()),
        Err(_) => Err(io::Error::new(
            io::ErrorKind::Interrupted,
            "Could not find paplay",
        )),
    }
}

fn fetch_audio_file() -> io::Result<path::PathBuf> {
    path::Path::new("/usr/share/sounds/freedesktop/stereo/alarm-clock-elapsed.oga").canonicalize()
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn play_audio() {
        assert!(fetch_audio_file().is_ok());
        assert_eq!((), ring_alarm().unwrap());
    }
}
