use std::{io, thread, time};

mod audio;
mod notifications;

const TIMERS: &[(i32, &str)] = &[(25, "🍅 Time to focus!"), (5, "🥱 Time to rest")];

pub struct Timer {
    cicles: i32,
    rests: i32,
}

impl Timer {
    pub fn new() -> Self {
        Self {
            cicles: 0,
            rests: 0,
        }
    }

    pub fn start(&mut self) -> io::Result<()> {
        Ok(for _ in 0..2 {
            Self::start_timer_and_notify_with(TIMERS[0].0 as u64, TIMERS[0].1)?;
            self.cicles += 1;
            audio::ring_alarm()?;
            Self::start_timer_and_notify_with((TIMERS[1].0 * self.cicles) as u64, TIMERS[1].1)?;
            self.rests += 1;
        })
    }

    fn start_timer_and_notify_with(minutes: u64, msg: &str) -> io::Result<()> {
        notifications::notify_with(msg)?;
        Ok(thread::sleep(Self::get_duration_of(minutes)))
    }

    #[allow(unreachable_code, unused_variables)]
    fn get_duration_of(minutes: u64) -> time::Duration {
        #[cfg(test)]
        {
            return time::Duration::new(1, 0);
        }
        time::Duration::new(minutes * 60, 0)
    }

    pub fn get_results(&self) -> io::Result<()> {
        audio::ring_alarm()?;
        notifications::notify_with(self.to_string().as_str())
    }

    fn to_string(&self) -> String {
        format!(
            "You've done {} cicles and have rested {} times",
            self.cicles, self.rests
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn inicialization() {
        let t = Timer::new();
        assert_eq!(
            t.to_string(),
            "You've done 0 cicles and have rested 0 times".to_string()
        );
    }

    #[test]
    fn basics() {
        let mut t = Timer::new();
        assert!(t.start().is_ok());
        assert!(t.get_results().is_ok());
    }
}
